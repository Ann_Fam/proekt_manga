<?php
require 'db.php';
$data = $_POST;
    $errors = array(); 
    if(isset($data['inputsub'])){
        if(trim($data['login'])==''){
            $errors[]= 'Введите логин!';
        }
        if(trim($data['login'])==''){
            $errors[]= 'Введите Email!';
        }
        if($data['nameuser']==''){
          $errors[]= 'Введите свое имя!';
      }
        if($data['password']==''){
            $errors[]= 'Введите пароль!';
        }
        if($data['password2']!=$data['password']){
            $errors[]='Пароли не совпадают!';
        }
        if(R::count('users', "login= ?",array($data['login']))>0)
        {
            $errors[]="Пользователь с таким логином существует!";
        }
        if(R::count('users', "email=?",array($data['email']))>0)
        {
            $errors[]="Пользователь с таким Email существует!";
        }
        if(empty($errors)){
            $user=R::dispense('users');
            $user->login = $data['login'];
            $user->email = $data['email'];
            $user->email = $data['nameuser'];
            $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
            if($data['login']=='admin' and $data['password']='admin')
            {
                $user->role = true;
            }
            else
            {
                $user->role = false;
            }
            R::store($user);
            $smsg="Регистрация прошла успешно!";
            
        }else{
            $fsmsg=array_shift($errors);
        }
    } 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Главная</title>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="css/mains.css">
  <link rel="stylesheet" href="css/dist/burger-menu.css">
  <link rel="stylesheet" href="css/MainPage.css">
</head>
<body>
<div class="header">
    <div class="logo"><a href="index.php">Ori</a></div>
    <div class="nav">
      <a href="index.php">Главная</a>
      <a href="projects.php">Проекты</a>
      <a href="contacts.php">Контакты</a>
      <?php if(isset($_SESSION['logged_user'])): ?>
      <a class="nav__link5" href="user.php"><?php echo $_SESSION['logged_user']->login;?></a>
      <a class="nav__link5" href="./logout.php">Выйти</a>
      <?php else :?>
      <a class="nav__link5" href="./autop.php">Вход</a>
      <?php endif ;?>
    </div>
    <div class="overlay">
      <nav class="overlayMenu">  
        <ul role="menu">
          <li><a href="index.php" role="menuitem">Главная</a></li>
          <li><a href="projects.php" role="menuitem">Проекты</a></li>
          <li><a href="contacts.php" role="menuitem">Контакты</a></li>
          <?php if(isset($_SESSION['logged_user'])): ?>
          <li><a class="nav__link5" href="user.php"><?php echo $_SESSION['logged_user']->login;?></a></li>
          <li><a class="nav__link5" href="./logout.php">Выйти</a></li>
      <?php else :?>
        <li><a class="nav__link5" href="./autop.php">Вход</a></li>
      <?php endif ;?>
        </ul> 
      </nav>
    </div>
  
    <div class="navBurger" role="navigation" id="navToggle"></div> 
  </div>

  

  <div class="content2">
    <div class="foto">
        <img class="tetka" src="images/tetka.jpg" alt="...">
    </div>
    <div class="regform">   
        <div class="reg"><span>Регистрация</span></div>
        <form class="formareg" method="POST">
        <?php if(isset($smsg)){?><div style="border: double; border-width: 1px; border-radius: 10px;" role="alert"> <?php echo $smsg; ?> </div><?php }?>
        <?php if(isset($fsmsg)){?><div style="border: double; border-width: 1px; border-radius: 10px;" role="alert" role="alert"> <?php echo $fsmsg; ?> </div><?php }?>
        <span>Логин</span>
        <input class="inputreg" name="login" type="text">
        <span>Имя</span>
        <input class="inputreg" name="nameuser" type="text">
        <span>Почта</span>
        <input class="inputreg" name="email" type="email">
        <span>Пароль</span>
        <input class="inputreg" name="password" type="password">
        <span>Повторите пароль</span>
        <input class="inputreg" name="password2" type="password">
        <div class="subbb">
        <input class="inputsub" type="submit" name="inputsub">
        </div>
        </form>
    </div>
    <div class="foto">
        <img class="tetka" src="images/blueeye.jpg" alt="...">
    </div>
  </div>

  <div class="footer">
    <img src="images/email.png" alt="" class="img-footer">
    <img src="images/vk.png" alt="" class="img-footer">
    <img src="images/fc.svg" alt="" class="img-footer">
    <img src="images/inst.png" alt="" class="img-footer">
  </div>

  <script>
    $("#navToggle").click(function () {
      $(this).toggleClass("active");
      $(".overlay").toggleClass("open");
      // this line ▼ prevents content scroll-behind
      $("body").toggleClass("locked");
    });

    $(".overlay a").click(function () {
      $("#navToggle").toggleClass("active");
      $(".overlay").toggleClass("open");
      $("body").toggleClass("locked");
    });
  </script>
</body>
</html>