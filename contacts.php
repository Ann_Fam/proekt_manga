<?php
require 'db.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Контакты</title>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="css/mains.css">
  <link rel="stylesheet" href="css/dist/burger-menu.css">
  <link rel="stylesheet" href="css/ContactsPage.css">
</head>
<body>
<div class="header">
    <div class="logo"><a href="index.php">Ori</a></div>
    <div class="nav">
      <a href="index.php">Главная</a>
      <a href="projects.php">Проекты</a>
      <a href="contacts.php" style="color: #F36312">Контакты</a>
      <?php if(isset($_SESSION['logged_user'])): ?>
      <a class="nav__link5" href="user.php"><?php echo $_SESSION['logged_user']->login;?></a>
      <a class="nav__link5" href="./logout.php">Выйти</a>
      <?php else :?>
      <a class="nav__link5" href="./autop.php">Вход</a>
      <?php endif ;?>
    </div>
    <div class="overlay">
      <nav class="overlayMenu">  
        <ul role="menu">
          <li><a href="index.php" role="menuitem">Главная</a></li>
          <li><a href="projects.php" role="menuitem">Проекты</a></li>
          <li><a href="contacts.php" role="menuitem" style="color: #F36312">Контакты</a></li>
          <?php if(isset($_SESSION['logged_user'])): ?>
          <li><a class="nav__link5" href="user.php"><?php echo $_SESSION['logged_user']->login;?></a></li>
          <li><a class="nav__link5" href="./logout.php">Выйти</a></li>
      <?php else :?>
        <li><a class="nav__link5" href="./autop.php">Вход</a></li>
      <?php endif ;?>
        </ul> 
      </nav>
    </div>
  
    <div class="navBurger" role="navigation" id="navToggle"></div> 
  </div>

  <div class="content">
    <img class="siluet" src="images/siluet.png" alt="...">
    <form>
      <span style="font-family: PTMono-Bold; margin: 15% 0;">Оставьте сообщение команде</span>
      <span class="text-for-input">Ваше имя</span>
      <input class="classic-row type="text" name="name">
      <span class="text-for-input">Ваш E-mail</span>
      <input class="classic-row" type="email" name="email">
      <span class="text-for-input">Сообщение</span>
      <textarea class="message-row" type="text" name="message"></textarea>
      <input type="submit" value="Отправить">
    </form>
    <img class="branch" src="images/picture branch.png" alt="...">
  </div>

  <div class="footer">
    <img src="images/email.png" alt="" class="img-footer">
    <img src="images/vk.png" alt="" class="img-footer">
    <img src="images/fc.svg" alt="" class="img-footer">
    <img src="images/inst.png" alt="" class="img-footer">
  </div>

  <script>
    $("#navToggle").click(function () {
      $(this).toggleClass("active");
      $(".overlay").toggleClass("open");
      // this line ▼ prevents content scroll-behind
      $("body").toggleClass("locked");
    });

    $(".overlay a").click(function () {
      $("#navToggle").toggleClass("active");
      $(".overlay").toggleClass("open");
      $("body").toggleClass("locked");
    });
  </script>
</body>
</html>