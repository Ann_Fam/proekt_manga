<?php 
require 'db.php';
$data=$_POST;
$errors=array();
$query=R::findAll('listmanga');
if(isset($data['Save'])){
      $userlist=R::dispense('listmanga');
      $userlist->titlemanga = $_POST['manga'];
      $userlist->op = $_POST['op'];
      R::store($userlist);
      header('Location: ' . $_SERVER['HTTP_REFERER']);
  
}
if (isset($_POST['edit-submit'])) {
  $aim = R::load('listmanga', $_GET['id']);
  $aim -> titlemanga = $_POST['edit_aim'];
  $aim->op = $_POST['op'];
  R::store($aim);
  header('Location: ' . $_SERVER['HTTP_REFERER']);
}
if (isset($_POST['delete_submit'])) {
  $mangatrash=R::load('listmanga', $_GET['id']);
  R::trash($mangatrash);

  header('Location: ' . $_SERVER['HTTP_REFERER']);
}
//
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Контакты</title>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="css/mains.css">
  <link rel="stylesheet" href="css/dist/burger-menu.css">
  <link rel="stylesheet" href="css/ContactsPage.css">
</head>
<body>
<div class="header">
    <div class="logo"><a href="index.php">Ori</a></div>
    <div class="nav">
      <a href="index.php">Главная</a>
      <a href="projects.php">Проекты</a>
      <a href="contacts.php">Контакты</a>
      <?php if(isset($_SESSION['logged_user'])): ?>
      <a class="nav__link5" href="user.php" style="color: #F36312"><?php echo $_SESSION['logged_user']->login;?></a>
      <a class="nav__link5" href="./logout.php">Выйти</a>
      <?php else :?>
      <a class="nav__link5" href="./autop.php">Вход</a>
      <?php endif ;?>
    </div>
    <div class="overlay">
      <nav class="overlayMenu">  
        <ul role="menu">
          <li><a href="index.php" role="menuitem">Главная</a></li>
          <li><a href="projects.php" role="menuitem">Проекты</a></li>
          <li><a href="contacts.php" role="menuitem">Контакты</a></li>
          <?php if(isset($_SESSION['logged_user'])): ?>
          <li><a class="nav__link5" href="user.php" style="color: #F36312"><?php echo $_SESSION['logged_user']->login;?></a></li>
          <li><a class="nav__link5" href="./logout.php">Выйти</a></li>
      <?php else :?>
        <li><a class="nav__link5" href="./autop.php">Вход</a></li>
      <?php endif ;?>
        </ul> 
      </nav>
    </div>
  
    <div class="navBurger" role="navigation" id="navToggle"></div> 
  </div>

  <div class="content4">
    <img class="siluet" src="images/siluet.png" alt="...">
    <a href="?create=1" class="btn btn-success btn-sm" data-toggle="modal" data-target="#createModal1?>" style="
    height: 33px;
"><i class="fa fa-plus-square" aria-hidden="true"></i></a>
    <table class="table table-striped table-hover mt-2">
					<thead class="table-white">
						<tr>
							<th>Название</th>
              <th>описание</th>
              <th>Удалить/Изменить</th>

						</tr>
					</thead>
					<tbody>
          
					<?php foreach ($query as $value) { ?>
            <tr>
							<td><?=$value['titlemanga'] ?></td>
              <td><?=$value['op'] ?></td>
							<td>
								<a href="?edit=<?=$value['id'] ?>" class="btn btn-success btn-sm" data-toggle="modal" data-target="#editModal<?=$value['id'] ?>"><i class="fa fa-edit"></i></a> 
								<a href="?delete=<?=$value['id'] ?>" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal<?=$value['id'] ?>"><i class="fa fa-trash"></i></a>
								<?php require 'modal.php'; ?>
							</td>
						</tr> <?php } ?>
					</tbody>
				</table>
        
        <img class="branch" src="images/picture branch.png" alt="...">
    </div>
  </div>
  <div class="footer">
    <img src="images/email.png" alt="" class="img-footer">
    <img src="images/vk.png" alt="" class="img-footer">
    <img src="images/fc.svg" alt="" class="img-footer">
    <img src="images/inst.png" alt="" class="img-footer">
  </div>

  <script>
    $("#navToggle").click(function () {
      $(this).toggleClass("active");
      $(".overlay").toggleClass("open");
      // this line ▼ prevents content scroll-behind
      $("body").toggleClass("locked");
    });

    $(".overlay a").click(function () {
      $("#navToggle").toggleClass("active");
      $(".overlay").toggleClass("open");
      $("body").toggleClass("locked");
    });
  </script>
</body>
</html>