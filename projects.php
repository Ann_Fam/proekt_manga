<?php
require 'db.php';
$roleuser=$_SESSION['logged_user']->role;
$data=$_POST;
if(isset($data['saveas'])){
  $mangaedit= R::findLike('mangalist' , array( 'position'=> array($data['position'])));
  foreach($mangaedit as $val1){
  if($data['position']!=''){
  if($data['title']!=''){
  $val1 -> title = $data['title'];
  }
  if($data['style']!=''){
  $val1 -> genre = $data['style'];
  }
  if($data['photo']!=''){
  $val1 -> photo = $data['photo'];
  }
  }
  R::store($val1);
  }
  header('Location: ' . $_SERVER['HTTP_REFERER']);
}
$pos1= R::findLike('mangalist' , array( 'position'=> 1));
foreach($pos1 as $position1){}
$pos2= R::findLike('mangalist' , array( 'position'=> 2));
foreach($pos2 as $position2){}
$pos3= R::findLike('mangalist' , array( 'position'=> 3));
foreach($pos3 as $position3){}
$pos4= R::findLike('mangalist' , array( 'position'=> 4));
foreach($pos4 as $position4){}
$pos5= R::findLike('mangalist' , array( 'position'=> 5));
foreach($pos5 as $position5){}
$pos6= R::findLike('mangalist' , array( 'position'=> 6));
foreach($pos6 as $position6){}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Проекты</title>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  
  <link rel="stylesheet" href="js/jquery.fancybox.js">
  <link rel="stylesheet" href="js/jquery.fancybox.min.js">
  <link rel="stylesheet" href="js/core.js">
  <script src="js/jquery.fancybox.js"></script>
  <script src="js/jquery.fancybox.min.js"></script>
  <script src="js/core.js"></script>

  <link rel="stylesheet" href="css/jquery.fancybox.css">
  <link rel="stylesheet" href="css/jquery.fancybox.min.css">

  <link rel="stylesheet" href="css/mains.css">
  <link rel="stylesheet" href="css/dist/burger-menu.css">
  <link rel="stylesheet" href="css/ProjectsPage.css">
</head>
<body>
<div class="header">
    <div class="logo"><a href="index.php">Ori</a></div>
    <div class="nav">
      <a href="index.php">Главная</a>
      <a href="projects.php" style="color: #F36312">Проекты</a>
      <a href="contacts.php">Контакты</a>
      <?php if(isset($_SESSION['logged_user'])): ?>
      <a class="nav__link5" href="user.php"><?php echo $_SESSION['logged_user']->login;?></a>
      <a class="nav__link5" href="./logout.php">Выйти</a>
      <?php else :?>
      <a class="nav__link5" href="./autop.php">Вход</a>
      <?php endif ;?>
    </div>
    <div class="overlay">
      <nav class="overlayMenu">  
        <ul role="menu">
          <li><a href="index.php" role="menuitem">Главная</a></li>
          <li><a href="projects.php" role="menuitem" style="color: #F36312">Проекты</a></li>
          <li><a href="contacts.php" role="menuitem">Контакты</a></li>
          <?php if(isset($_SESSION['logged_user'])): ?>
          <li><a class="nav__link5" href="user.php"><?php echo $_SESSION['logged_user']->login;?></a></li>
          <li><a class="nav__link5" href="./logout.php">Выйти</a></li>
      <?php else :?>
        <li><a class="nav__link5" href="./autop.php">Вход</a></li>
      <?php endif ;?>
        </ul> 
      </nav>
    </div>
  
    <div class="navBurger" role="navigation" id="navToggle"></div> 
  </div>
  <?php if($roleuser==true) :?>
    <form method="POST">
      <input type="text" name="title" placeholder="Изменить название">
          <input type="text" name="style" placeholder="Изменить жанр">
          <input name="position" list="positionlist" placeholder="Выберите позицию" />
          <datalist id="positionlist">
            <option value="1" />
            <option value="2" />
            <option value="3" />
            <option value="4" />
            <option value="5" />
            <option value="6" />
          </datalist>
          <input type="file" name="photo" multiple accept="image/*,image/jpeg">
          <input type="submit"name="saveas" value="Изменить">
       </form>
  <div class="content">
    <div class="column">
      <div class="text">
        <span>Последние обновления</span>
      </div>
      <div class="card">
        <a data-fancybox="imagesnew" data-caption="Моя любовь в сети" href="images/projects/new/<?php echo $position1['photo'] ?>">
          <img src="images/projects/new/<?php echo $position1['photo'] ?>" alt="" />
        </a>
        <div class="under-card-text">
          <span style="font-family: PTMono-Bold;"><?php echo $position1['title'];?></span>
          <span>Жанр: <?php echo $position1['genre'];?></span>
          <span>Позиция: <?php echo $position1['position'];?></span>
        </div>
      </div>
      <div class="card">
        <a data-fancybox="imagesnew" data-caption="Клинок, рассекающий демонов" href="images/projects/new/<?php echo $position2['photo'] ?>">
          <img src="images/projects/new/<?php echo $position2['photo'] ?>" alt="">
        </a>
        <div class="under-card-text">
          <span style="font-family: PTMono-Bold;"><?php echo $position2['title'];?></span>
          <span>Жанр: <?php echo $position2['genre'];?></span>
          <span>Позиция: <?php echo $position2['position'];?></span>
        </div>
      </div>
    </div>
    <div class="column">
      <div class="text">
        <span>Горячие <br> новинки</span>
      </div>
      <div class="card">
        <a data-fancybox="imageshot" data-caption="Выживание с нуля" href="images/projects/hot/<?php echo $position3['photo'] ?>">
          <img src="images/projects/hot/<?php echo $position3['photo'] ?>" alt="">
        </a>
        <div class="under-card-text">
          <span style="font-family: PTMono-Bold;"><?php echo $position3['title'];?></span>
          <span>Жанр: <?php echo $position3['genre'];?></span>
          <span>Позиция: <?php echo $position3['position'];?></span>
        </div>
      </div>
      <div class="card">
        <a data-fancybox="imageshot" data-caption="Выживание с нуля" href="images/projects/hot/<?php echo $position4['photo'] ?>">
          <img src="images/projects/hot/<?php echo $position4['photo'] ?>" alt="">
        </a>
        <div class="under-card-text">
          <span style="font-family: PTMono-Bold;"><?php echo $position4['title'];?></span>
          <span>Жанр: <?php echo $position4['genre'];?></span>
          <span>Позиция: <?php echo $position4['position'];?></span>
        </div>
      </div>
    </div>
    <div class="column">
      <div class="text">
        <span>Прочее</span>
      </div>
      <div class="card">
        <a data-fancybox="imagesother" data-caption="Поднятие уровня в одиночку" href="images/projects/other/<?php echo $position5['photo'] ?>">
          <img src="images/projects/other/<?php echo $position5['photo'] ?>" alt="">
        </a>
        <div class="under-card-text">
          <span style="font-family: PTMono-Bold;"><?php echo $position5['title'];?></span>
          <span>Жанр: <?php echo $position5['genre'];?></span>
          <span>Позиция: <?php echo $position5['position'];?></span>
        </div>
      </div>
      <div class="card">
        <a data-fancybox="imagesother" data-caption="Поднятие уровня в одиночку" href="images/projects/other/<?php echo $position6['photo'] ?>">
          <img src="images/projects/other/<?php echo $position6['photo'] ?>" alt="">
        </a>
        <div class="under-card-text">
          <span style="font-family: PTMono-Bold;"><?php echo $position6['title'];?></span>
          <span>Жанр: <?php echo $position6['genre'];?></span>
          <span>Позиция: <?php echo $position6['position'];?></span>
        </div>
      </div>
    </div>
  </div>
  <?php  else: ?>
    <div class="content">
    <div class="column">
      <div class="text">
        <span>Последние обновления</span>
      </div>
      <div class="card">
        <a data-fancybox="imagesnew" data-caption="Моя любовь в сети" href="images/projects/new/<?php echo $position1['photo'] ?>">
          <img src="images/projects/new/<?php echo $position1['photo'] ?>" alt="" />
        </a>
        <div class="under-card-text">
          <span style="font-family: PTMono-Bold;"><?php echo $position1['title'];?></span>
          <span>Жанр: <?php echo $position1['genre'];?></span>
        </div>
      </div>
      <div class="card">
        <a data-fancybox="imagesnew" data-caption="Клинок, рассекающий демонов" href="images/projects/new/<?php echo $position2['photo'] ?>">
          <img src="images/projects/new/<?php echo $position2['photo'] ?>" alt="">
        </a>
        <div class="under-card-text">
          <span style="font-family: PTMono-Bold;"><?php echo $position2['title'];?></span>
          <span>Жанр: <?php echo $position2['genre'];?></span>
        </div>
      </div>
    </div>
    <div class="column">
      <div class="text">
        <span>Горячие <br> новинки</span>
      </div>
      <div class="card">
        <a data-fancybox="imageshot" data-caption="Выживание с нуля" href="images/projects/hot/<?php echo $position3['photo'] ?>">
          <img src="images/projects/hot/<?php echo $position3['photo'] ?>" alt="">
        </a>
        <div class="under-card-text">
          <span style="font-family: PTMono-Bold;"><?php echo $position3['title'];?></span>
          <span>Жанр: <?php echo $position3['genre'];?></span>
        </div>
      </div>
      <div class="card">
        <a data-fancybox="imageshot" data-caption="Выживание с нуля" href="images/projects/hot/<?php echo $position4['photo'] ?>">
          <img src="images/projects/hot/<?php echo $position4['photo'] ?>" alt="">
        </a>
        <div class="under-card-text">
          <span style="font-family: PTMono-Bold;"><?php echo $position4['title'];?></span>
          <span>Жанр: <?php echo $position4['genre'];?></span>
        </div>
      </div>
    </div>
    <div class="column">
      <div class="text">
        <span>Прочее</span>
      </div>
      <div class="card">
        <a data-fancybox="imagesother" data-caption="Поднятие уровня в одиночку" href="images/projects/other/<?php echo $position5['photo'] ?>">
          <img src="images/projects/other/<?php echo $position5['photo'] ?>" alt="">
        </a>
        <div class="under-card-text">
          <span style="font-family: PTMono-Bold;"><?php echo $position5['title'];?></span>
          <span>Жанр: <?php echo $position5['genre'];?></span>
        </div>
      </div>
      <div class="card">
        <a data-fancybox="imagesother" data-caption="Поднятие уровня в одиночку" href="images/projects/other/<?php echo $position6['photo'] ?>">
          <img src="images/projects/other/<?php echo $position6['photo'] ?>" alt="">
        </a>
        <div class="under-card-text">
          <span style="font-family: PTMono-Bold;"><?php echo $position6['title'];?></span>
          <span>Жанр: <?php echo $position6['genre'];?></span>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>
  <div class="footer">
    <img src="images/email.png" alt="" class="img-footer">
    <img src="images/vk.png" alt="" class="img-footer">
    <img src="images/fc.svg" alt="" class="img-footer">
    <img src="images/inst.png" alt="" class="img-footer">
  </div>

  <script>
    $("#navToggle").click(function () {
      $(this).toggleClass("active");
      $(".overlay").toggleClass("open");
      // this line ▼ prevents content scroll-behind
      $("body").toggleClass("locked");
    });

    $(".overlay a").click(function () {
      $("#navToggle").toggleClass("active");
      $(".overlay").toggleClass("open");
      $("body").toggleClass("locked");
    });
  </script>
</body>
</html>