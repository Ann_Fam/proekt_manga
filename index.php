<?php  require 'db.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Главная</title>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="css/mains.css">
  <link rel="stylesheet" href="css/dist/burger-menu.css">
  <link rel="stylesheet" href="css/MainPage.css">
</head>
<body>
  <div class="header">
    <div class="logo"><a href="index.php">Ori</a></div>
    <div class="nav">
      <a href="index.php" style="color: #F36312">Главная</a>
      <a href="projects.php">Проекты</a>
      <a href="contacts.php">Контакты</a>
      <?php if(isset($_SESSION['logged_user'])): ?>
      <a class="nav__link5" href="user.php"><?php echo $_SESSION['logged_user']->login;?></a>
      <a class="nav__link5" href="./logout.php">Выйти</a>
      <?php else :?>
      <a class="nav__link5" href="./autop.php">Вход</a>
      <?php endif ;?>
    </div>
    <div class="overlay">
      <nav class="overlayMenu">  
        <ul role="menu">
          <li><a href="index.php" role="menuitem" style="color: #F36312">Главная</a></li>
          <li><a href="projects.php" role="menuitem">Проекты</a></li>
          <li><a href="contacts.php" role="menuitem">Контакты</a></li>
          <?php if(isset($_SESSION['logged_user'])): ?>
          <li><a class="nav__link5" href="user.php"><?php echo $_SESSION['logged_user']->login;?></a></li>
          <li><a class="nav__link5" href="./logout.php">Выйти</a></li>
      <?php else :?>
        <li><a class="nav__link5" href="./autop.php">Вход</a></li>
      <?php endif ;?>
        </ul> 
      </nav>
    </div>
  
    <div class="navBurger" role="navigation" id="navToggle"></div> 
  </div>

  

  <div class="content">
    <div class="aboutUs">
      <img src="../images/angelbg.jpg" alt="...">
      <div class="text">
        <span class="up">О нас</span>
        <span class="down"> Мы команда по переводу манги "Ori". <br><br>
          Наша сильнейшая любовь к манге в итоге вылилась в эту команду по переводу! :) <br> <br>
          Мы переводим как мангу, так и комиксы, манхву, вебтуны и маньхуа. Ищем веселых и интересных людей! Мы не ограничиваем себя рамками в жанрах - хотите переводить яой или юри? С удовольствием предоставим вам это!</span>
      </div>
    </div>
    <div class="team">
      <div class="ol">
        <img src="images/ol.png" alt="ol">
      </div>
      <div class="team-text">
        <span style="font-family: PTMono-Bold">Действуюший состав команды</span>
        <span>Клинеры: Akane, Hide <br>
          Переводчики: Makoto, Yuri <br>
          Редакторы: Nagisa, Tomiko <br>
          Тайперы: Misaki, Kasumi <br>
          Эдитор: Kasumi <br>
          Бета: Fana
        </span>

        <span style="font-family: PTMono-Bold; margin-top: 60px;">Вы можете попасть в нашу команду</span>
        <span>Нам теребуются опытные: клинеры,
          переводчики, редакторы, тайперы, эдиторы.
          Уточнять вопросы в контактах
        </span>
      </div>
    </div>
  </div>

  <div class="footer">
    <img src="images/email.png" alt="" class="img-footer">
    <img src="images/vk.png" alt="" class="img-footer">
    <img src="images/fc.svg" alt="" class="img-footer">
    <img src="images/inst.png" alt="" class="img-footer">
  </div>

  <script>
    $("#navToggle").click(function () {
      $(this).toggleClass("active");
      $(".overlay").toggleClass("open");
      // this line ▼ prevents content scroll-behind
      $("body").toggleClass("locked");
    });

    $(".overlay a").click(function () {
      $("#navToggle").toggleClass("active");
      $(".overlay").toggleClass("open");
      $("body").toggleClass("locked");
    });
  </script>
</body>
</html>